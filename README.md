# Fiche de suivi

## 5 Avril
- Continuation Rapport et Diaporama du Projet

## 2 Avril
- Finition sur l'éditeur
- Commencement Rapport et Diaporama du Projet

## 29 Mars
- Règlage de plusieurs bugs dans l'éditeur (il devient presque entièrement fonctionnel)
- Modification de la développeurs map pour découvrir/corriger des bugs
- Continuation de correction du bug de déplacement

## 26 Mars
- Découverte d'un bug mineur dans le menu
- Bug de déplacements découle de plusieurs bugs comme :  les tiles ne s'initialisent pas correctement
- Amélioration du GUI

## 25 Mars
- Réponse des développeurs -> reflexion sur le projet
- Test de godot 4.2
- Regard sur comment est codée Tank of Freedom 2

## 22 Mars
- Prise de contact avec les développeurs
- Recherche sur le bug de déplacements (Normalement il n'est pas possible d'aller dans la forêt hors la c'est possible)
- Correction de l'utilisation des touches claviers dans l'éditeur

## 19 Mars
- Installation Godot 4.2 et Tank of freedom2
- Test du jeu Tank of freedom 2 
- test de gameplay sur le jeu et découverte du code de jeu
- Découverte Gros changement entre TOF1 et TOF2, graphique 2D -> 3D

## 18 Mars
- Reflexion sur le projet (continuer le portage, règler des bugs sur tank of freedom 2, recommencer le jeu de 0)
- Correction du menu dans l'éditeur (tiles, move map)
- Ajout de bug dans le doc de bug

## 15 Mars 
Soutenance de projet : 
- Finission des diapo de la soutenance
- Entrainement à l'oral (sur ce que chacun doit dire)
- Fin de recherche du dossier contenant les tips (retrouver dans godot 2)
- Correction de l'affichage de la tool box

## 12 Mars 
- Réalisation des documents de gestion d'entreprise (OBS, Gestion de risque, Gestion des équipes).
- Recherche du fichier contenant les "tips".
- Debut de recherche afin de meiux comprendre le problème de l'éditeur de niveau.

## 11 Mars
- Réalisation des documents de gestion d'entreprise (SWOT, PBS et WBS).
- Quelques fixations de l'affichage utilisateur.
- Réalisation du diapo de la soutenance de mi-projet.


## 4 Mars
- Résolution de certains problèmes d'UI. 
- Bon avancement sur l'éditeur de map (partie très longue puisque rien ne fonctionnait).

Fin de séance :
Continuer la correction de bugs.


## 19 Février
- Mise en place de la méthode de travail.
- On a presque "rattraper" l'état d'avancement du projet de l'année dernière (il nous reste les mêmes bugs qu'eux).

Fin de séance : 
Prochaine séance, on continue la correction de bugs

## 12 Février
- On décide de repartir de Godot 2 pour faire le portage en s'aidant du projet de l'année derniere pour une meilleure compréhension de Godot et du projet. 
- Titouan est à l'aise sur Godot, il est explique au reste de l'équipe pour apprendre plus vite.
- Romain joue pour se familiariser avec le jeu.

Fin de séance : 
Prochaine séance, on commence à faire le portage sur Godot 3.

## 29 Janvier
- Découverte du projet et du jeu Tanks of Freedom.
- Lecture du Git de l'état du projet de l'année derniere.
- Installation du jeu et de Godot.
- Début d'apprentissage du langage Godot.

Fin de séance :
Est-ce qu'on reprend le projet de l'année derniere ou repars de la version 2 de Godot ?

